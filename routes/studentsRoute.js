var express = require('express');
var router = express.Router();
const studentsController = require('../controllers/studentsController');

router.get ('/', studentsController.index);
router.get ('/:id', studentsController.find);
router.post ('/', studentsController.store);
router.put ('/:id', studentsController.update);
router.delete ('/:id', studentsController.destroy);

module.exports = router;