var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const studentsRouter = require('./routes/studentsRoute');
const teacherRouter = require('./routes/teacherRoute');
const noteRouter = require('./routes/noteRoute');
const filiereRouter = require('./routes/filiereRoute');
const matiereRouter = require('./routes/matiereRoute');
const coeffRouter = require('./routes/coeffRoute');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

const mongoose = require('mongoose');

main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/my-api-app');
  console.log("DataBase is connected");

}

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/students', studentsRouter)
app.use('/teachers', teacherRouter);
app.use('/notes', noteRouter);
app.use('/filieres', filiereRouter);
app.use('/matieres', matiereRouter);
app.use('/coeff', coeffRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
